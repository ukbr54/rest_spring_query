package com.example.criteriaAPi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class UserController {

    @Autowired
    private IUserDAO userDAO;

    @GetMapping(value = "/users")
    public List<User> searchUser(@RequestParam(value = "search",required = false) String search){
        List<SearchCriteria> params = new ArrayList<>();
        if(search != null){
            /**
             *  \w	Any word character (letter, number, underscore)
             *  a+	One or more of a
             *  a?	Zero or one of a
             */
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
            Matcher matcher = pattern.matcher(search + ",");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3)));
            }
        }
        return userDAO.searchUser(params);
    }
}
