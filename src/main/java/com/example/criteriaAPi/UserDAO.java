package com.example.criteriaAPi;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class UserDAO implements IUserDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<User> searchUser(List<SearchCriteria> params) {

        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<User> query = builder.createQuery(User.class);
        final Root<User> from = query.from(User.class);

        Predicate predicate = builder.conjunction(); //and
        for(SearchCriteria param : params){
            if(param.getOperation().equalsIgnoreCase(">"))

                predicate = builder.and(predicate,builder.greaterThanOrEqualTo(
                        from.get(param.getKey()),param.getValue().toString())
                );

            else if(param.getOperation().equalsIgnoreCase("<"))

                predicate = builder.and(predicate,builder.lessThanOrEqualTo(
                        from.get(param.getKey()),param.getValue().toString())
                );

            else if(param.getOperation().equalsIgnoreCase(":")){
                String containsLikePattern = getContainsLikePattern(param.getValue().toString());
                predicate = builder.and(predicate,builder.like(
                        builder.lower(from.get(param.getKey())),containsLikePattern)
                );

            }
        }

        query.where(predicate);
        List<User> users = entityManager.createQuery(query).getResultList();

        return users;
    }

    private static String getContainsLikePattern(String searchTerm) {
        if (searchTerm == null || searchTerm.isEmpty()) {
            return "%";
        }
        else {
            return "%" + searchTerm.toLowerCase() + "%";
        }
    }

    @Override
    public void save(User entity) {
        entityManager.persist(entity);
    }


    // SELECT u FROM User u;
    @Override
    public List<User> listAllUser(){
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        final Root<User> root = query.from(User.class);

        query.select(root);

        TypedQuery<User> typedQuery = entityManager.createQuery(query);
        List<User> userList = typedQuery.getResultList();

        return userList;
    }

    @Override
    public List<User> listUserWhoseAgeIsGreaterThanParamValue(Integer age) {

        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<User> q = builder.createQuery(User.class);
        Root<User> r = q.from(User.class);

        ParameterExpression<Integer> parameter = builder.parameter(Integer.class);
        q.select(r).where(builder.gt(r.get("age"),parameter));

        TypedQuery<User> query = entityManager.createQuery(q);
        query.setParameter(parameter,age);

        List<User> resultList = query.getResultList();

        return resultList;
    }
}
