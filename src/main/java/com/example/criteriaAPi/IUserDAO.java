package com.example.criteriaAPi;

import java.util.List;

public interface IUserDAO {

    List<User> searchUser(final List<SearchCriteria> params);

    void save(final User entity);

    List<User> listAllUser();

    List<User> listUserWhoseAgeIsGreaterThanParamValue(Integer age);
}
