package com.example.criteriaAPi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * key: used to hold field name – for example: firstName, age, … etc.
 * operation: used to hold the operation – for example: Equality, less than, … etc.
 * value: used to hold the field value – for example: john, 25, … etc.
 */

@Setter
@Getter
@AllArgsConstructor
public class SearchCriteria {

    private String key;
    private String operation;
    private Object value;
}
