package com.example.specification;

import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class UserSpecificationBuilder {

    private final List<SearchCriteria> params;

    public UserSpecificationBuilder() {
        this.params = new ArrayList<>();
    }

    public UserSpecificationBuilder with(String key,String operation,Object value){
        params.add(new SearchCriteria(key,operation,value));
        return this;
    }

    public Specification<MyUser> build(){
        if(params.size() == 0) return null;
        for(int i=1;i<params.size();i++){

        }
    }
}
