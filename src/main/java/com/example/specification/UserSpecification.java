package com.example.specification;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class UserSpecification implements Specification<MyUser> {

    private SearchCriteria searchCriteria;

    @Override
    public Predicate toPredicate
            (Root<MyUser> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        if(searchCriteria.getOperation().equals(">")){
            return criteriaBuilder.greaterThanOrEqualTo(
                   root.<String>get(searchCriteria.getKey()),searchCriteria.getValue().toString()
            );
        }else if(searchCriteria.getOperation().equals("<")){
            return criteriaBuilder.lessThanOrEqualTo(
                   root.<String>get(searchCriteria.getKey()),searchCriteria.getValue().toString()
            );
        }else if(searchCriteria.getOperation().equals(":")){
            if(root.get(searchCriteria.getKey()).getJavaType() == String.class){
                return criteriaBuilder.like(
                  criteriaBuilder.lower(root.<String>get(searchCriteria.getKey())),searchCriteria.getValue().toString()
                );
            }else{
                return criteriaBuilder.equal(
                  root.<String>get(searchCriteria.getKey()),searchCriteria.getValue().toString()
                );
            }
        }
        return null;
    }
}
