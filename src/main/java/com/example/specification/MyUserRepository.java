package com.example.specification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MyUserRepository extends JpaRepository<MyUser,Long>, JpaSpecificationExecutor<MyUser> {

}
