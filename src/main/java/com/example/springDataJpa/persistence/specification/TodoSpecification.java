package com.example.springDataJpa.persistence.specification;

import com.example.springDataJpa.persistence.model.Todo;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import java.util.Optional;

@NoArgsConstructor(access =  AccessLevel.PRIVATE)
public final class TodoSpecification {

    public static Specification<Todo> titleOrDescriptionContainsIgnoreCase(String searchItem){
        return (root,query,cb) ->{
            String search = getContainsLikePattern(searchItem);
            return cb.or(
                cb.like(cb.lower(root.get("title")),search),
                cb.like(cb.lower(root.get("description")),search)
            );
        };
    }

    private static String getContainsLikePattern(String searchItem){
        return  Optional.ofNullable(searchItem)
                        .filter(s -> !(StringUtils.isEmpty(s)))
                        .map(search -> "%" + search.toLowerCase() + "%")
                        .orElseGet(() -> "%");

    }
}
