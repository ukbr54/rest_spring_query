package com.example.springDataJpa.persistence.model;

import com.example.springDataJpa.common.Precondition;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Setter
@Getter
@Entity
@ToString
@Table(name = "todos")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EntityListeners(AuditingEntityListener.class)
public final class Todo {

    public static final int MAX_LENGTH_DESCRIPTION = 500;
    public static final int MAX_LENGTH_TITLE = 100;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description",length = MAX_LENGTH_DESCRIPTION)
    private String description;

    @Column(name = "title",length = MAX_LENGTH_TITLE, nullable = false)
    private String title;

    @Version
    private Long version;

    @Column(name = "creation_time", nullable = false)
    @CreatedDate
    private ZonedDateTime creationTime;

    @Column(name = "modification_time")
    @LastModifiedDate
    private ZonedDateTime modificationTime;

    private Todo(Builder builder){
        this.title = builder.title;
        this.description = builder.description;
    }

    public Todo(String description, String title) {
        this.description = description;
        this.title = title;
    }

    public static Builder getBuilder(){
        return new Builder();
    }

    public void update(String newTitle,String newDescription){
        validateTitleAndDescription(newTitle,newDescription);
        this.title = newTitle;
        this.description = newDescription;
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Builder {

        private String title;
        private String description;

        public Builder description(String description){
            this.description = description;
            return this;
        }

        public Builder title(String title){
            this.title = title;
            return this;
        }

        public Todo build(){
            Todo build = new Todo(this);
            build.validateTitleAndDescription(build.getTitle(),build.getDescription());
            return build;
        }
    }

    private void validateTitleAndDescription(String title,String description){
        Precondition.notEmpty(title,"Title cannot be empty");
        Precondition.notNull(title,"Title cannot be null");

        Precondition.isTrue(title.length() <= MAX_LENGTH_TITLE,
                "The Maximum length of the title is <%d> characters.",MAX_LENGTH_TITLE);

        Precondition.isTrue((description == null || description.length() <= MAX_LENGTH_DESCRIPTION),
                "The maximum length of the description is <%d> characters.",MAX_LENGTH_DESCRIPTION);
    }
}
