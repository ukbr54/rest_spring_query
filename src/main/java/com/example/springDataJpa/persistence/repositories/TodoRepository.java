package com.example.springDataJpa.persistence.repositories;

import com.example.springDataJpa.persistence.model.Todo;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.stream.Stream;

@Repository
public interface TodoRepository extends BaseRepository<Todo,Long>, JpaSpecificationExecutor<Todo> {

    @Query("SELECT t.title FROM Todo t WHERE t.id = :id")
    String findTitleById(@Param("id") Long id);

    @Query("SELECT t.title FROM Todo t WHERE t.id = :id")
    Optional<String> findTitleByIdUsingOptional(@Param("id") Long id);

    @Query("SELECT t from Todo t")
    Stream<Todo> streamAllTodos();

    @Async
    @Query("SELECT t.title FROM Todo t where t.id = :id")
    Future<String> findTitleByIdUsingFuture(@Param("id") Long id);

    List<Todo> findByDescriptionContainsOrTitleContainsAllIgnoreCase(String description,String title);

    @Query("SELECT t FROM Todo t WHERE LOWER(t.title) LIKE LOWER('%', :searchTerm, '%') OR LOWER(t.description) LIKE LOWER('%', :searchTerm, '%')")
    List<Todo> findBySearchTerm(@Param("searchTerm") String searchTerm);

    List<Todo> findAll(Sort sort);

}
