package com.example.springDataJpa.dtos;

import com.example.springDataJpa.dtos.TodoDTO;
import com.example.springDataJpa.persistence.model.Todo;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

final public class TodoMapper {

    public static TodoDTO mapEntityIntoDTO(Todo todo){
        TodoDTO dto = new TodoDTO();
        BeanUtils.copyProperties(todo,dto);
        return dto;
    }

    public static List<TodoDTO> mapEntityIntoDTOs(Iterable<Todo> todos){
        List<TodoDTO> dtoList = new ArrayList<>();
        todos.forEach(todo ->  dtoList.add(mapEntityIntoDTO(todo)));
        return  dtoList;
    }
}
