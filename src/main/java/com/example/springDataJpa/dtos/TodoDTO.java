package com.example.springDataJpa.dtos;

import com.example.springDataJpa.persistence.model.Todo;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@Setter
@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public final class TodoDTO {

    private Long id;

    @NotEmpty
    @Size(max = Todo.MAX_LENGTH_TITLE)
    private String title;

    @Size(max = Todo.MAX_LENGTH_DESCRIPTION)
    private String description;

    private ZonedDateTime creationTime;

    private ZonedDateTime modificationTime;
}
