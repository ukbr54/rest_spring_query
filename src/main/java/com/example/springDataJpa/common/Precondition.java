package com.example.springDataJpa.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Precondition {

    public static void isTrue(boolean expression,String errorMessageTemplate,Object... errorMessageArguments){
        isTrue(expression,String.format(errorMessageTemplate,errorMessageArguments));
    }

    /**
     *
     * @param expression
     * @param errorMessage
     * if the expression is false.
     * @throws java.lang.IllegalArgumentException if the inspected expression is false.
     */
    public static void isTrue(boolean expression,String errorMessage){
       if(!expression)
           throw new IllegalArgumentException(errorMessage);
    }

    public static void notEmpty(String string,String errorMessage){
        if(string.isEmpty())
            throw new IllegalArgumentException(errorMessage);
    }

    public static void notNull(String string,String errorMessage){
        if(Objects.isNull(string))
            throw new IllegalArgumentException(errorMessage);
    }
}
