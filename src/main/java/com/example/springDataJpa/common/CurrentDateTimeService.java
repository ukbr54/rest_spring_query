package com.example.springDataJpa.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Slf4j
@Component
public class CurrentDateTimeService implements DateTimeService{

    @Override
    public ZonedDateTime getCurrentDateAndTime() {
        ZonedDateTime currentDateAndTime = ZonedDateTime.now();
        log.info("Returning current date and time: {}", currentDateAndTime);
        return currentDateAndTime;
    }
}
