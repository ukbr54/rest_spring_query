package com.example.springDataJpa.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.util.ClassUtils;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Properties;

/**
 * The next mandatory step is registering the class as an auto-configuration candidate, by adding the name of the class under the key
 * org.springframework.boot.autoconfigure.EnableAutoConfiguration in the standard file resources/META-INF/spring.factories:
 *
 *      org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
 *      com.baeldung.autoconfiguration.MySQLAutoconfiguration
 *
 * If we want our auto-configuration class to have priority over other auto-configuration candidates, we can add the
 * @AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE) annotation
 *
 * Class conditions allow us to specify that a configuration bean will be included if a specified class is present using the
 * @ConditionalOnClass annotation, or if a class is absent using the @ConditionalOnMissingClass annotation.
 *
 */

@Configuration
@ConditionalOnClass(DataSource.class)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
@EnableJpaAuditing(dateTimeProviderRef = "dateTimeProvider")
public class PersistenceConfiguration {

    @Autowired
    private Environment env;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value(("${spring.datasource.password}"))
    private String password;

    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;

    @Bean(name = "dataSource")
    @ConditionalOnMissingBean
    public DataSource dataSource(){
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    /**
     * If we want to include a bean only if a specified bean is present or not, we can
     * use the @ConditionalOnBean and @ConditionalOnMissingBean annotations.
     * @return
     */
    @Bean
    @ConditionalOnBean(name = "dataSource")
    @ConditionalOnMissingBean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("com.example");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        if(additionalProperties() != null)
            em.setJpaProperties(additionalProperties());
        return em;
    }

    /**
     * Let’s also configure a transactionManager bean that will only be loaded if a bean of type JpaTransactionManager
     * is not already defined:
     */
    @Bean
    @ConditionalOnMissingBean(type = "JpaTransactionManager")
    JpaTransactionManager transactionManager(){
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    @Conditional(HibernateCondition.class)
    final Properties additionalProperties() {
        final Properties hibernateProperties = new Properties();

        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("h2-hibernate.hbm2ddl.auto"));
        hibernateProperties.setProperty("hibernate.dialect", env.getProperty("h2-hibernate.dialect"));

        return hibernateProperties;
    }

    static class HibernateCondition extends SpringBootCondition {

        private static final String[] CLASS_NAMES = { "org.hibernate.ejb.HibernateEntityManager", "org.hibernate.jpa.HibernateEntityManager" };

        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
            ConditionMessage.Builder message = ConditionMessage.forCondition("Hibernate");

            return Arrays.stream(CLASS_NAMES)
                     .filter(className -> ClassUtils.isPresent(className, context.getClassLoader()))
                     .map(className -> ConditionOutcome.match(message.found("class").items(ConditionMessage.Style.NORMAL, className))).findAny()
                     .orElseGet(() -> ConditionOutcome.noMatch(message.didNotFind("class", "classes").items(ConditionMessage.Style.NORMAL, Arrays.asList(CLASS_NAMES))));
        }

    }

}
