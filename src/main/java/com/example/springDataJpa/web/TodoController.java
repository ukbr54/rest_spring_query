package com.example.springDataJpa.web;

import com.example.springDataJpa.service.ITodoSearchService;
import com.example.springDataJpa.service.ITodoService;
import com.example.springDataJpa.persistence.model.Todo;
import com.example.springDataJpa.dtos.TodoDTO;
import com.example.springDataJpa.persistence.repositories.TodoRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Stream;

@Slf4j
@RestController
@RequestMapping("/api/todos")
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class TodoController {

    ITodoService todoService;

    ITodoSearchService searchService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    TodoDTO create(@RequestBody @Valid TodoDTO todoDTO){
        log.info("Creating a new todo entry by using information: {}", todoDTO);
        TodoDTO created = todoService.create(todoDTO);
        log.info("Created a new todo entry: {}", created);
        return created;
    }


    @GetMapping("/{id}")
    TodoDTO getTodo(@PathVariable("id") Long id)  {
        TodoDTO todoDTO = todoService.findById(id);
        return todoDTO;
    }

    @DeleteMapping("/{id}")
    TodoDTO delete(@PathVariable("id") Long id){
        TodoDTO todoDTO = todoService.delete(id);
        return todoDTO;
    }

    @GetMapping
    List<TodoDTO> findAll(){
        return todoService.findAll();
    }

    @PutMapping
    TodoDTO update(@RequestBody @Valid TodoDTO todoDTO){
        return todoService.update(todoDTO);
    }

    @GetMapping
    List<TodoDTO> search(@RequestParam("search") String search){
        List<TodoDTO> todoDTOS = searchService.search(search);
        return todoDTOS;
    }
}
