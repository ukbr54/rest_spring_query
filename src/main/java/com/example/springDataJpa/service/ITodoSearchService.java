package com.example.springDataJpa.service;

import com.example.springDataJpa.dtos.TodoDTO;

import java.util.List;

public interface ITodoSearchService {

    List<TodoDTO> search(String searchItem);
}
