package com.example.springDataJpa.service;

import com.example.springDataJpa.common.exception.ResourceNotFoundException;
import com.example.springDataJpa.dtos.TodoDTO;
import com.example.springDataJpa.dtos.TodoMapper;
import com.example.springDataJpa.persistence.model.Todo;
import com.example.springDataJpa.persistence.repositories.TodoRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class TodoServiceImpl implements ITodoService{

    private final TodoRepository todoRepository;

    @Override
    @Transactional
    public TodoDTO create(TodoDTO todo) {
        log.info("Creating a new todo entry by using information: {}", todo);
        Todo created = Todo.getBuilder()
                .title(todo.getTitle())
                .description(todo.getDescription())
                .build();
        created = todoRepository.save(created);
        log.info("Created a new todo entry: {}", created);
        return TodoMapper.mapEntityIntoDTO(created);
    }

    @Override
    public TodoDTO delete(Long id) {
        return null;
    }

    @Override
    public List<TodoDTO> findAll() {

        return null;
    }

    @Override
    public TodoDTO findById(Long id) {
        log.info("Finding Todo entry by using id: {}",id);
        Todo todo = todoRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Resource Not Found!"));
        log.info("Found todo entry: {}",todo);
        return TodoMapper.mapEntityIntoDTO(todo);
    }

    @Override
    public TodoDTO update(TodoDTO updatedTodoEntry) {
        return null;
    }
}
