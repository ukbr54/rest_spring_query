package com.example.springDataJpa.service;

import com.example.springDataJpa.dtos.TodoDTO;

import java.util.List;

public interface ITodoService {

    TodoDTO create(TodoDTO todo);

    TodoDTO delete(Long id);

    List<TodoDTO> findAll();

    TodoDTO findById(Long id);

    TodoDTO update(TodoDTO updatedTodoEntry);

}
