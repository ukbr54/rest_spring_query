package com.example.springDataJpa.service;

import com.example.springDataJpa.dtos.TodoDTO;
import com.example.springDataJpa.dtos.TodoMapper;
import com.example.springDataJpa.persistence.model.Todo;
import com.example.springDataJpa.persistence.repositories.TodoRepository;
import com.example.springDataJpa.persistence.specification.TodoSpecification;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class TodoSearchServiceImpl implements ITodoSearchService{

    TodoRepository todoRepository;

    @Override
    @Transactional(readOnly = true)
    public List<TodoDTO> search(String searchItem) {
        Specification<Todo> searchSpec = TodoSpecification.titleOrDescriptionContainsIgnoreCase(searchItem);
        List<Todo> todos = todoRepository.findAll(searchSpec);
        return TodoMapper.mapEntityIntoDTOs(todos);
    }
}
