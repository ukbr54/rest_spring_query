package com.example;

import com.example.criteriaAPi.IUserDAO;
import com.example.criteriaAPi.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HomeController {

    @Autowired private IUserDAO userDAO;

    @GetMapping("/")
    public String root(){
        return "Hello World";
    }

    @GetMapping("/list-all")
    public List<User> listAllUser(){
        return userDAO.listAllUser();
    }

    @GetMapping("/list-user-gt")
    public List<User> listAllUserWhoseAreGreaterThanParam(@RequestParam("age") Integer age){
        return userDAO.listUserWhoseAgeIsGreaterThanParamValue(age);
    }
}
