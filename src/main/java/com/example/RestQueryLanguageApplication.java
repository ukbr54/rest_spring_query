package com.example;

import com.example.criteriaAPi.UserDAO;
import com.example.criteriaAPi.User;
import com.example.springDataJpa.common.AuditingDateTimeProvider;
import com.example.springDataJpa.common.DateTimeService;
import com.example.springDataJpa.persistence.model.Todo;
import com.example.springDataJpa.persistence.repositories.TodoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.auditing.DateTimeProvider;

@SpringBootApplication
public class RestQueryLanguageApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestQueryLanguageApplication.class, args);
	}

	@Bean
	DateTimeProvider dateTimeProvider(DateTimeService dateTimeService) {
		return new AuditingDateTimeProvider(dateTimeService);
	}

	@Bean
	CommandLineRunner init(UserDAO userDAO, TodoRepository todoRepository){
		return  args -> {

			todoRepository.deleteAll();

			User userJohn = new User("John","Doe","john@doe.com",22);
			userDAO.save(userJohn);

			User userTom = new User("Tom","Doe","tom@doe.com",26);
			userDAO.save(userTom);

			Todo todo1 = new Todo("Java 8 feature like lambda,Java date and time api","Java");
			todoRepository.save(todo1);

			Todo todo2 = new Todo("Complete all feature of java 8 ","Java");
			todoRepository.save(todo2);

		};
	}
}
